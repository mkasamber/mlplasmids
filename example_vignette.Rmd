---
title: "ML plasmids vignettes"
author: "Sergio Arredondo-Alonso"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

# Introduction
 
**mlplasmids** is a binary classifier to predict either as plasmid-derived or chromosome-derived. The provided classifier is a **Support Vector Machine** which was previously optimized and compared with other existing machine-learning techniques (Logistic regression, Random Forest...). 

# Requirements

- R (tested in version 3.3.3)
- Biostrings
- mlr 
- seqinr 

Devtools will search for the above packages in the library path. If some package(s) are not found, devtools will download and automatically compile required packages. 

# Installation 

The first version is available through our gitlab repository. 

```{r installation, eval=FALSE}
install.packages("devtools")
devtools::install_git("sirarredondo/mlplasmids")
```

Loading the library

```{r}
library(mlplasmids)
```


## Package help

**mlplasmids** contains a single function called **plasmid_classification**, you can learn more about arguments and values required using the help page.

```{r}
help("plasmid_classification")
```

# Usage

## Simple usage

- Point the path to the fasta file with the contig sequences. If the name does not contain relative or full path then the name of the file will be relative to current working directory.

- The value returned by the function is a dataframe reporting predicted plasmid-derived contigs. 
```{r simple_usage}
# Change the following object with the system location of your input file
my_path <- ('~/Data/E_faecium_plasmids/E745_control/E745_spades_contigs.fasta')
df_default_prediction <- plasmid_classification(path_input_file = my_path)
head(df_default_prediction)

```

## Setting the probabilities threshold

By default the classifier assigns each object(contig) to the class(plasmid or chromosome) with a highest posterior probability. The default threshold (0.5) can be changed using the argument **prob_threshold**. 

```{r prob_threshold}
df_prob_prediction <- plasmid_classification(path_input_file = my_path, prob_threshold = 0.7)
head(df_prob_prediction)
```

## Reporting chromosome-predicted contigs

Report contigs classified as chromosome-derived using the argument **full_output**. 

```{r full_output}
df_full_prediction <- plasmid_classification(path_input_file = my_path, full_output = TRUE)
head(df_full_prediction)
```





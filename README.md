mlplasmids documentation
================

<p align="center">
<img src="data/logo.png" alt="logo_package" width="600">
</p>

Introduction
============

**mlplasmids** consists of binary classifiers to predict contigs either as plasmid-derived or chromosome-derived. 
It currently classifies short-read contigs as chromosomes and plasmids for *Enterococcus faecium*, *Escherichia coli* and *Klebsiella pneumoniae*. Further species will be added in the future. The provided classifiers are **Support Vector Machines** which were previously optimized and selected versus other existing machine-learning techniques (Logistic regression, Random Forest..., see also [Reference]()). The classifiers use pentamer frequencies (n = 1,024) to infer whether a particular contig is plasmid- or chromosome- derived.

<img src="data/input_output.png" width="500px" style="display: block; margin: auto;" />

Requirements
============

-   R (tested in version 3.3.3)
-   [devtools](https://cran.r-project.org/web/packages/devtools/index.html)

Dependencies
============

-   [kernlab](https://cran.r-project.org/web/packages/kernlab/index.html)
-   [Biostrings](https://bioconductor.org/packages/release/bioc/html/Biostrings.html)
-   [mlr](https://cran.r-project.org/web/packages/kernlab/index.html)
-   [seqinr](https://cran.rstudio.com/web/packages/seqinr/index.html)

Devtools will search for the above packages in your library path. If some package(s) are not found, devtools will download and automatically compile required packages.

Installation
============

The R package is available through a gitlab repository.

``` r
install.packages("devtools")
devtools::install_git("https://gitlab.com/sirarredondo/mlplasmids")
```

Depending on your internet connection, it might take a while to download the package as it contains >300Mb of data.

``` r
library(mlplasmids)
```

Shiny app implementation
------------------------

**mlplasmids** has also been implemented as a user-friendly **Shiny app.**

-   [Link to Shiny app.](https://sarredondo.shinyapps.io/mlplasmids/)

<img src="data/shiny_app.png" width="800px" style="display: block; margin: auto;" />

Usage
=====

Simple usage
------------

-   You only have to specify the **path** pointing to your **fasta file** with the contig sequences. If the name does not contain relative or full path then the name of the file will be relative to current working directory.

-   The **value returned** by the function is a **dataframe** reporting predicted plasmid-derived contigs.

``` r
# Change the following object with the system location of your input file
my_path <- ('~/Data/E_faecium_plasmids/E745_control/E745_spades_contigs.fasta')
example_prediction <- plasmid_classification(path_input_file = my_path, species = 'Enterococcus faecium')
```

``` r
head(example_prediction)
```

    ##    Prob_Chromosome Prob_Plasmid Prediction
    ## 36     0.042683241    0.9573168    Plasmid
    ## 41     0.006876928    0.9931231    Plasmid
    ## 43     0.085699794    0.9143002    Plasmid
    ## 46     0.034722838    0.9652772    Plasmid
    ## 55     0.013318272    0.9866817    Plasmid
    ## 63     0.012378171    0.9876218    Plasmid
    ##                                Contig_name Contig_length
    ## 36  NODE_36_length_22926_cov_74.4686_ID_71         22926
    ## 41  NODE_41_length_19157_cov_27.6651_ID_81         19157
    ## 43  NODE_43_length_18357_cov_78.2779_ID_85         18357
    ## 46  NODE_46_length_16769_cov_25.6735_ID_91         16769
    ## 55 NODE_55_length_14069_cov_23.9568_ID_109         14069
    ## 63  NODE_63_length_11179_cov_33.185_ID_125         11179

### Species model

Currently, we have developed two Support Vector Machine models for predicting plasmid- derived sequences for *Enterococcus faecium*, *Klebsiella pneumoniae* and *Escherichia coli*.

-   The argument **species** is used to define which model to use.

``` r
example_prediction <- plasmid_classification(path_input_file = my_path, species = "Enterococcus faecium")
```

Output explanation
------------------

| Prob. Chromosome | Prob.Plasmid | Prediction | Contig Name | Contig length |
|:----------------:|:------------:|:----------:|:-----------:|:-------------:|
|       0.92       |     0.08     | Chromosome |  Contig\_12 |     22544     |
|       0.18       |     0.82     |   Plasmid  |  Contig\_33 |      7144     |

-   *Prob\_Chromosome*: **Posterior probability** for a particular object (contig) of belonging to the **chromosome class**.
-   *Prob\_Plasmid*: **Posterior probability** for a particular object (contig) of belonging to the **plasmid class**.
-   *Prediction*: **Class** (Plasmid or Chromosome) **assigned** to a particular object (contig)
-   *Contig\_name*: Name of the contig (parsed from the header)
-   *Contig\_length*: Length of the contig

Setting the probabilities threshold
-----------------------------------

By default the classifier assigns each object(contig) to the class(plasmid or chromosome) with a highest posterior probability. We can actually change the threshold (0.5) and e.g. only report plasmid-predicted contigs with a posterior probability higher than 0.7 using the argument **prob\_threshold**.

``` r
df_prob_prediction <- plasmid_classification(path_input_file = my_path, prob_threshold = 0.7, species = "Enterococcus faecium")
```

``` r
head(df_prob_prediction)
```

    ##    Prob_Chromosome Prob_Plasmid Prediction
    ## 36     0.042683241    0.9573168    Plasmid
    ## 41     0.006876928    0.9931231    Plasmid
    ## 43     0.085699794    0.9143002    Plasmid
    ## 46     0.034722838    0.9652772    Plasmid
    ## 55     0.013318272    0.9866817    Plasmid
    ## 63     0.012378171    0.9876218    Plasmid
    ##                                Contig_name Contig_length
    ## 36  NODE_36_length_22926_cov_74.4686_ID_71         22926
    ## 41  NODE_41_length_19157_cov_27.6651_ID_81         19157
    ## 43  NODE_43_length_18357_cov_78.2779_ID_85         18357
    ## 46  NODE_46_length_16769_cov_25.6735_ID_91         16769
    ## 55 NODE_55_length_14069_cov_23.9568_ID_109         14069
    ## 63  NODE_63_length_11179_cov_33.185_ID_125         11179

Reporting chromosome-predicted contigs
--------------------------------------

We can actually report contigs classified as chromosome-derived using the argument **full\_output**.

``` r
df_full_prediction <- plasmid_classification(path_input_file = my_path, full_output = TRUE, species = "Enterococcus faecium")
```

``` r
head(df_full_prediction)
```

    ##   Prob_Chromosome Prob_Plasmid Prediction
    ## 1       0.9942573  0.005742706 Chromosome
    ## 2       0.9952094  0.004790560 Chromosome
    ## 3       0.9877517  0.012248315 Chromosome
    ## 4       0.9782258  0.021774203 Chromosome
    ## 5       0.9916892  0.008310834 Chromosome
    ## 6       0.9949595  0.005040522 Chromosome
    ##                              Contig_name Contig_length
    ## 1  NODE_1_length_114323_cov_39.1165_ID_1        114323
    ## 2  NODE_2_length_113077_cov_39.4702_ID_3        113077
    ## 3  NODE_3_length_107352_cov_40.4857_ID_5        107352
    ## 4  NODE_4_length_106722_cov_38.9931_ID_7        106722
    ## 5  NODE_5_length_100348_cov_39.3503_ID_9        100348
    ## 6 NODE_6_length_100017_cov_38.5794_ID_11        100017

Minimum contig length
---------------------

By default we have defined a contig lenght threshold of 1,000 bp to report the prediction of the models. At your own risk you can set this threshold lower using the argument **min\_length**

``` r
df_full_prediction <- plasmid_classification(path_input_file = my_path, full_output = TRUE, species = "Enterococcus faecium", min_length = 500)
```

    ## Warning in plasmid_classification(path_input_file = my_path, full_output
    ## = TRUE, : !!! Be aware that predictions for sequences < 1000 bp can be
    ## misleading !!!

Help
----

**mlplasmids** only contains a single function called **plasmid\_classification**, you can learn more about arguments and values required using the help page.

``` r
help('plasmid_classification')
```

Advanced unix users installation/usage
======================================

Thanks to the fantastic work of @nickp60, mlplasmids can be installed and used without the requirement to start R/Rstudio. This can be ideal if you are intending to use mlplasmids on a high-performance-computing facility or 
to integrate mlplasmids in your pipeline. Using your unix terminal, you can first download the gitlab repo:

``` bash
git clone https://gitlab.com/sirarredondo/mlplasmids.git
cd mlplasmids/
```

The script 'run\_mlplasmids.R' (scripts/run\_mlplasmids.R) will first check whether mlplasmids is already present in your R libraries or if it needs to be installed. 
You need to provide three arguments

-   1st argument: Input path of your fasta file
-   2nd argument: Output path in which mlplasmids will create and store the results (.tab)
-   3rd argument: Integer number ranging from 0 to 1 corresponding to the minimum probability threshold to classify a sequence as chromosome- or plasmid-derived.
-   4th argument: Character value. Species with plasmid classifiers available. You need to specify one of the following three species: 'Enterococcus faecium','Klebsiella pneumoniae' or 'Escherichia coli')

To predict the complete genome of *E. faecium* AUS0004 (GCA\_000250945.1), run

``` bash
Rscript scripts/run_mlplasmids.R data/GCA_000250945.1_ASM25094v1_genomic.fna.gz examples/mlplasmids_results.tab 0.7 'Enterococcus faecium'
```

    ## [1] "Threshold: 0.7"
    ## [1] "Species: Enterococcus faecium"

Check the prediction

``` bash
cat examples/mlplasmids_results.tab
```

    ## "Prob_Chromosome"    "Prob_Plasmid"  "Prediction"    "Contig_name"   "Contig_length"
    ## 0.988873107398901    0.011126892601099   "Chromosome"    "CP003351.1 Enterococcus faecium Aus0004, complete genome"  2955294
    ## 0.0251878316908554   0.974812168309145   "Plasmid"   "CP003352.1 Enterococcus faecium Aus0004 plasmid AUS0004_p1, complete sequence" 56520
    ## 0.0621990343052241   0.937800965694776   "Plasmid"   "CP003353.1 Enterococcus faecium Aus0004 plasmid AUS0004_p2, complete sequence" 3847
    ## 0.03024578432515 0.96975421567485    "Plasmid"   "CP003354.1 Enterococcus faecium Aus0004 plasmid AUS0004_p3, complete sequence" 4119

Troubleshooting
===============

Devtools
--------

If installation fails you can try to install some required libraries in your command-line terminal (bash):

``` bash
sudo apt-get install build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev
```

Biostrings error
----------------

In some new versions of R, you will get the following message:

*Biostrings not available for R (version)*

You can install Biostrings using bioClite

``` r
source("http://bioconductor.org/biocLite.R")
biocLite("Biostrings")
```

Biostrings is required to calculate the pentamer frequencies (n=1024) for each contig.

Issues/Bugs
===========

You can report any issues or bugs that you find while installing/running **mlplasmids** using the [Issue tracker](https://gitlab.com/sirarredondo/mlplasmids/issues)

Citation
========

If you use **mlplasmids** package for your research please cite the following publication:

Arredondo-Alonso et al., Microbial Genomics 2018;4 DOI 10.1099/mgen.0.000224

Future plans
============

You can contribute to the development of **mlplasmids** in different ways:

-   If you have optimised a plasmid classifier and you would like to use mlplasmids implementation to make it available.
-   If you have a bunch of **complete genomes from a particular bacterial species** with short-read WGS data available and you would like to create a new plasmid model.

In any case, you can always send us an email:

-   Sergio Arredondo-Alonso - <S.ArredondoAlonso@umcutrecht.nl>
-   Anita C. Schürch - <A.C.Schurch@umcutrecht.nl>
